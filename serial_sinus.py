#!/usr/bin/python3

import time
import serial
import math
import signal 

freq = 0.1
lower_limit = 40
upper_limit = 10
ser = serial.Serial('/dev/ttyACM0', 2000000, timeout=0.0)

if not (ser.is_open):
    print("Serial error")
    exit(1)

lastTime = time.time()

def exit_handler(a,b):
    global ser
    ser.close()

signal.signal(signal.SIGINT, exit_handler)

time.sleep(2.0)

while True:
    data = ser.readline()[:-2] #the last bit gets rid of the new-line chars
    if data:
       print ("SER:", str(data))
#    if (time.time() - lastTime > 0.01):
    lastTime = time.time()
    dimValue = int(lower_limit + (upper_limit - lower_limit)/2 + math.sin(time.time()*freq*2*math.pi)*(upper_limit - lower_limit)/2)

    print(dimValue)
    ser.write(bytes([dimValue]))
#        print(str(ser.write(bytes([dimValue]))), " bytes written")
#        ser.flush()
    time.sleep(0.01)
